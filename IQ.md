# **I/Q SIGNALS**

- I/Q or *in phase* and *quadrature* refer to two sinusoids that have the same frequency and are 90 DEG out of phase. 

- Mostly the I signal is a Cosine and Q signal is a Sine wave.
They are always amplitude modulated not Frequency modulated. They are done using an I/Q modulator.

- I/Q modulation is done by multiplying I/Q waveforms by modulating signals that can have negative voltage values, and consequently the “amplitude” modulation can result in a 180° phase shift.

- Any form of modulation can be performed simply by varying the amplitude—only the amplitude—of I and Q signals, and then adding them together.

- Phase modulation can be conveniently achieved by varying the amplitude of I/Q signals. Increasing the amplitude of one of the waveforms relative to the other causes the summation signal to shift toward the higher-amplitude waveform

- Quadrature modulation refers to modulation that involves I/Q signals.Quadrature phase shift keying can be accomplished by adding I and Q carriers that have been individually multiplied, in accordance with the incoming digital data, by +1 or –1.