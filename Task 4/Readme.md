# **Summary for task 4**

The challenge was interesting learned to use GNU Radio and generated flow graph. The task was to create flowgraphs of AM transmitter and reciever. I genrated the flowgraphs and tested them using a WAV file.

### Screenshots 

![Transmitter](https://gitlab.com/CB_JR/challenges/-/raw/main/Task%204/Transmitter.png)
![Reciever](https://gitlab.com/CB_JR/challenges/-/raw/main/Task%204/Reciever.png)
![Output](https://gitlab.com/CB_JR/challenges/-/raw/main/Task%204/Output.png)
