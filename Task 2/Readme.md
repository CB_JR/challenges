# **Summary for task 2**

- Circuit were same as task 1 but to improve the buttons function we have used attach interrupt.

### Simulation link

[Task 2](https://www.tinkercad.com/things/13osOA4ENAg-task-2-/editel?sharecode=Z2ukd8dQYJjF3UCRMgCMtVS0eJnx9SE6-oSD6grLGYY)

## **Screenshot**

![Task 2](https://gitlab.com/CB_JR/challenges/-/raw/main/Task%202/Task_2.png)
