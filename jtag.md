#  **JTAG**

- It is an industry standard for verifying designs and testing PCBs. It specifies the use of a dedicated debug port implementing a serial communications interface for low-overhead access without requiring direct external access to the system address and data buses.
- The interface connects to an on-chip Test Access Port (TAP) that implements a stateful protocol to access a set of test registers that present chip logic levels and device capabilities of various parts.
### **Attack Scenarios**

- Attackers can get access to the microcontrollers memmory registers. They can manipulate the vaules in the register to bypass protection to make changes to the regitry areas that the original developer might have locked.
- The attacker can debug the system to find more vulnerabilities and make use of it to perform and attack 
- Through the access port the attacker can get the firmware from the device patch it and re flash it.