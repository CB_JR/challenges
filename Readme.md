# **Summary of all tasks**

*I have competed 6 out of 6 challenges that was given. All task screenshots and code has been pushed to the repository and writeups has been written in markdown language.*

| Challenege | Link  |
| ----------- | ----------- |
| Task 1  | [Task 1](https://gitlab.com/CB_JR/challenges/-/tree/main/Task%201) |
| Task 2  | [Task 2](https://gitlab.com/CB_JR/challenges/-/tree/main/Task%202) |
| Task 3  | [Task 3](https://gitlab.com/CB_JR/challenges/-/blob/main/jtag.md) |
| Task 4  | [Task 4](https://gitlab.com/CB_JR/challenges/-/tree/main/Task%204)|
| Task 5  | [Task 5](https://gitlab.com/CB_JR/challenges/-/blob/main/IQ.md) |
| Task 6  | [Task 6](https://gitlab.com/CB_JR/challenges/-/blob/main/relay___replay.md) |
