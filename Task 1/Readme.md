# **Summary for task 1**

- As for the task it was good .I had some fun doing it. Learned basics of registers and how to modify them. Schmitt trigger was used for hardware debouncing without the inverter in the end. 

### Simulation link

[Task 1](https://www.tinkercad.com/things/jjArIVMSOOy-task-1/editel?sharecode=vhZ9D-TEcLB2t3808spUWtvAJkEo9iSUmpxJgYYjJIA)

## **Screenshot**

![Task 1](https://gitlab.com/CB_JR/challenges/-/raw/main/Task%201/Task_1.png)

